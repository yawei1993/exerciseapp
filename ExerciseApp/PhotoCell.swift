//
//  PhotoCell.swift
//  ExerciseApp
//
//  Created by Vinnie Liu on 10/5/18.
//  Copyright © 2018 Yawei Liu. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet  weak var containerView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var imageTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
    }
    
    var photo: Photo? {
        didSet {
            if let photo = photo {
                imageView.image = photo.image
                imageTitleLabel.text = photo.title
            }
        }
    }
    
}
