//
//  DetailViewController.swift
//  ExerciseApp
//
//  Created by Vinnie Liu on 16/5/18.
//  Copyright © 2018 Yawei Liu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var image: UIImage!
    var desc: String?
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descLabel: UILabel!
    
    override func viewDidLoad() {
        self.imageView.image = image
        self.descLabel.text = desc
        super.viewDidLoad()        
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
        } else {
            print("Portrait")
        }
    }
}
