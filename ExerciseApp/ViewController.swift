//
//  ViewController.swift
//  ExerciseApp
//
//  Created by Vinnie Liu on 10/5/18.
//  Copyright © 2018 Yawei Liu. All rights reserved.
//

import UIKit
import SwiftSpinner

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topTitleLabel: UILabel!
    var photoArray = [Photo]()
    var chosenPhoto: Photo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        //below statements are used to add a navigation button on the top right corner for refreshing
        let rightButton = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(self.reloadCollectionView))
        self.navigationItem.rightBarButtonItem = rightButton
        getPhotoData()
    }
    
    func reloadCollectionView()
    {
        print("Reload button clicked")
        self.photoArray.removeAll()
        self.getPhotoData()
    }
    
    func getPhotoData()
    {
        SwiftSpinner.show("Fetching photos...")
        //create the url with URL
        let url = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")! //change the url
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "GET" //set http method as GET
        
        //Here we specify the Content-Type in order to correctly fetch the data result
        request.addValue("text/plain; charset=ISO-8859-1", forHTTPHeaderField: "Content-Type")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
        
            if data == nil{
                return
            }
            //here we have to encode the data result to convert them into meaningful data
            let decodedResult = String(data: data!, encoding: .isoLatin1)
            
            //here we change the format of the data result for the future JSON packaging/unpackaging usage
            let changedData = decodedResult?.data(using: .utf8)
            
            do {
                //create json object from data
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: changedData!, options: []) as? NSDictionary
                {
                    // Print out dictionary to have a glance
                    print(convertedJsonIntoDict)
                    self.decode(convertedJsonIntoDict: convertedJsonIntoDict)
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    func parseJson(from data: Data) {
        
        //here we have to encode the data result to convert them into meaningful data
        let decodedResult = String(data: data, encoding: .isoLatin1)
        
        //here we change the format of the data result for the future JSON packaging/unpackaging usage
        let changedData = decodedResult?.data(using: .utf8)
        
        do {
            //create json object from data
            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: changedData!, options: []) as? NSDictionary
            {
                // Print out dictionary to have a glance
                print(convertedJsonIntoDict)
                self.decode(convertedJsonIntoDict: convertedJsonIntoDict)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    //this method is used to extract useful values from the long json result
    func decode(convertedJsonIntoDict: NSDictionary)
    {
        //extract the title value
        let title = convertedJsonIntoDict["title"] as? String
        
        if let photos = convertedJsonIntoDict["rows"] as? [[String: Any]]
        {
            print(photos.count)
            
            for index in 0...photos.count-1
            {
                let aObject = photos[index]
                var imageTitle = aObject["title"] as? String
                var imageDescription = aObject["description"] as? String
                var imageURL = aObject["imageHref"] as? String
                if imageURL == nil
                {
                    print("there is NO url for this photo")
                    if imageDescription == nil && imageTitle != nil
                    {
                        print("this photo only have title")
                        let photo = Photo(title: imageTitle!)
                        self.photoArray.append(photo)
                        print("No url photo added")
                    }
                    if imageDescription == nil && imageTitle == nil
                    {
                        print("this photo has nothing")
                        let photo = Photo()
                        self.photoArray.append(photo)
                        print("No url photo added")
                    }
                    if imageDescription != nil && imageTitle != nil
                    {
                        print("this photo has title and description")
                        let photo = Photo(title: imageTitle!, description: imageDescription!)
                        self.photoArray.append(photo)
                        print("No url photo added")
                        
                    }
                    if imageDescription != nil && imageTitle == nil
                    {
                        print("this photo only have description")
                        let photo = Photo(description: imageDescription!)
                        self.photoArray.append(photo)
                        print("No url photo added")
                    }
                }
                else
                {
                        print(imageURL)
                        print("there is url for this photo")
                        let url = URL(string: imageURL!)
                        print(url)
                        
                        print("Download Started")
                        
                        self.getDataFromUrl(url: url!) { data, response, error in
                            
                            if let e = error {
                                //displaying the message
                                print("Error Occurred: \(e)")
                                DispatchQueue.main.async() {
                                    self.promptMessage(title: "Oops", message: "Cannot download this image, please try again later")
                                    print("Download error")
                                    
                                }
                            }
                            else {
                                //in case of now error, checking wheather the response is nil or not
                                if (response as? HTTPURLResponse) != nil {
                                    
                                    
                                    //checking if the response contains an image
                                    if let imageData = data {
                                        
                                        //getting the image
                                        let image = UIImage(data: imageData)
                                        
                                        if image != nil
                                        {
                                            if imageDescription == nil && imageTitle != nil
                                            {
                                                print("this photo has url and title")
                                                let photo = Photo(title: imageTitle!, image: image!)
                                                self.photoArray.append(photo)
                                            }
                                            if imageDescription == nil && imageTitle == nil
                                            {
                                                print("this photo only have url")
                                                let photo = Photo(image: image!)
                                                self.photoArray.append(photo)
                                            }
                                            if imageDescription != nil && imageTitle != nil
                                            {
                                                print("this photo has everything")
                                                let photo = Photo(title: imageTitle!, description: imageDescription!, image: image!)
                                                self.photoArray.append(photo)
                                            }
                                            if imageDescription != nil && imageTitle == nil
                                            {
                                                print("this photo  has url and description")
                                                let photo = Photo(description: imageDescription!, image: image!)
                                                self.photoArray.append(photo)
                                            }
                                            //displaying the image
                                            DispatchQueue.main.async() {
                                                self.collectionView.reloadData()
                                                print("photo array contains \(self.photoArray.count) photos now")
                                            }
                                        }
                                    } else {
                                        print("Image file is currupted")
                                    }
                                } else {
                                    print("No response from server")
                                }
                            }
                        }
                }
                defer
                {
                    // Update UI in a background thread
                    DispatchQueue.main.async {
                        self.topTitleLabel.text = title
                        self.collectionView.reloadData()
                         SwiftSpinner.hide()
                        print("photo array contains \(self.photoArray.count) photos now")
                    }
                }
            }
        }
    }
    
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            
            }.resume()
    }
    
    func promptMessage(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("there are \(self.photoArray.count) items")
        return self.photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.imageView.image = self.photoArray[indexPath.item].image
        cell.imageTitleLabel.text = self.photoArray[indexPath.item].title
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = self.photoArray[indexPath.item].image
        self.chosenPhoto = self.photoArray[indexPath.item]
        self.performSegue(withIdentifier: "ShowDetail", sender: image)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let image = self.photoArray[indexPath.item].image!
        let aspectRatio = image.size.height/image.size.width
        //Below methods are used to resize the image if the image's width is larger than the screen
        if image.size.width > UIScreen.main.bounds.width
        {
            let newWidth = UIScreen.main.bounds.width
            let newHeight = image.size.height * aspectRatio
            let targetSize = CGSize(width: newHeight, height: newWidth)
            let resizedImage = image.resizeImage(targetSize: targetSize)
            return resizedImage.size
        }
        
        return image.size
    }
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ShowDetail" {
            let detailVC = segue.destination as! DetailViewController
            detailVC.image = sender as! UIImage
            detailVC.desc = self.chosenPhoto?.description
            detailVC.navigationItem.title = self.chosenPhoto?.title
        }
    }
}


//MARK: - Method used to resize image if image is too large and go outside of the screen
extension UIImage {
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

