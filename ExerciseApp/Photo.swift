//
//  Photo.swift
//  ExerciseApp
//
//  Created by Vinnie Liu on 10/5/18.
//  Copyright © 2018 Yawei Liu. All rights reserved.
//

import Foundation
import UIKit

class Photo{
    var title: String?
    var description: String?
    var image: UIImage?
    
    init()
    {
        self.title = "No Title"
        self.description = "No Description"
        self.image = #imageLiteral(resourceName: "default-image")
    }
    
    init(title: String)
    {
        self.title = title
        self.description = "No Description"
        self.image = #imageLiteral(resourceName: "default-image")
    }
    
    init(title: String, description: String)
    {
        self.title = title
        self.description = description
        self.image = #imageLiteral(resourceName: "default-image")
    }
    
    init(title: String, description: String, image: UIImage)
    {
        self.title = title
        self.description = description
        self.image = image
    }
    
    init(title: String, image: UIImage) {
        self.title = title
        self.description = "No Description"
        self.image = image
    }

    init(description: String)
    {
        self.title = "No Title"
        self.description = description
        self.image = #imageLiteral(resourceName: "default-image")
    }
    
    init(image: UIImage)
    {
        self.title = "No Title"
        self.description = "No Description"
        self.image = image
    }
    
    init(description: String, image: UIImage)
    {
        self.title = "No Title"
        self.description = description
        self.image = image
    }
    
}
